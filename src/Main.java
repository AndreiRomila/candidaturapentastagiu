import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.printf("Enter a Value:  ");
		int a = in.nextInt();
		System.out.printf("Enter b Value:  ");
		int b = in.nextInt();
		Interval interval=new Interval(a, b);
		int val =interval.getMaxim();
		if (val == 0) {
			System.out.println(
					"Nu exista numere prime in acest interval care sa poata fi scrise ca o suma de numere prime diferita de el insusi.");
		} else
			System.out.println("The result is " + val);
	}
}
