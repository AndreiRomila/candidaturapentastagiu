
public class Interval {
	private int min;
	private int max;
	private PrimesVector vector;
	public Interval(int a, int b){
		if (b < a) {
			b = a ^ b;
			a = a ^ b;
			b = a ^ b;
		}
		this.min=a;
		this.max=b;
		this.vector=new PrimesVector(this.max);
		this.vector.generatePrimesVector();
	}

	public int getMaxim() {
		int nr = vector.getNumber(vector.getNumberOfPrimes()-1);
		int index = 0;
		while (vector.getNumber(index++) < min)
			;
		int max = getMaxTermsOfSum(vector.getNumber(index));
		int return_val = vector.getNumber(index++);
		while (index < nr) {
			int val = getMaxTermsOfSum(vector.getNumber(index));
			if (val >= max) {
				max = val;
				return_val = vector.getNumber(index);
			}
			index++;
		}
		return return_val;
	}
	private int getMaxTermsOfSum(int x) {
		int sum = 0;
		int k = 0;
		int first = 0;
		while (sum < x) {
			sum += vector.getNumber(k++);
		}
		if (sum == x) {
			return k;
		} else {
			while (vector.getNumber(k) < x) {
				while (sum > x) {
					sum -= vector.getNumber(first++);
				}
				if (sum == x) {
					return k - first;
				}
				sum += vector.getNumber(k++);
			}
			return 0;
		}
	}
}
