
public class PrimesVector {
	private int last_number;
	private int[] primes;
	private int number_of_primes;
	public PrimesVector(int last_number){
		this.last_number=last_number;
		primes=new int[last_number];
		number_of_primes=1;
	}
	public void generatePrimesVector(){
		char[] p = new char[last_number+1];
		int i, j;
		int k = 1;
		for (i = 3; i <= last_number; i = i + 2) {
			if (p[i] == 0) {
				number_of_primes++;
				for (j = i * i; j <= last_number; j += i << 1) {
					p[j] = 1;
				}
			}
		}
		primes[0] = 2;
		for (i = 3; i <= last_number; i = i + 2) {
			if (p[i] == 0) {
				primes[k++] = i;
			}
		}
	}
	public int getNumberOfPrimes(){
		return number_of_primes;
	}
	public int getNumber(int i){
		return primes[i];
	}
}
